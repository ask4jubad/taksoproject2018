defmodule Takso.Repo.Migrations.AddUserAndStatusToBooking do
  use Ecto.Migration

  def change do
    alter table(:sales_bookings) do
      add :status, :string, default: "open"
      add :user_id, references(:accounts_users)
    end
  end
end
