defmodule Takso.Repo.Migrations.CreateSalesAllocations do
  use Ecto.Migration

  def change do
    create table(:sales_allocations) do
      add :status, :string
      add :booking_id, references(:sales_bookings, on_delete: :nothing)
      add :taxi_id, references(:sales_taxis, on_delete: :nothing)

      timestamps()
    end

    create index(:sales_allocations, [:booking_id])
    create index(:sales_allocations, [:taxi_id])
  end
end
