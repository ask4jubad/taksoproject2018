defmodule Takso.Sales.Booking do
  use Ecto.Schema
  import Ecto.Changeset


  schema "sales_bookings" do
    field :dropoff_address, :string
    field :pickup_address, :string

    field :status, :string, default: "open"
    belongs_to :user, Takso.Accounts.User

    timestamps()
  end

  @doc false
  def changeset(booking, attrs) do
    booking
    |> cast(attrs, [:pickup_address, :dropoff_address])
    |> validate_required([:pickup_address, :dropoff_address])
  end
end
