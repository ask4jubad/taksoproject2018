defmodule Takso.Sales.Taxi do
  use Ecto.Schema
  import Ecto.Changeset


  schema "sales_taxis" do
    field :location, :string
    field :status, :string
    field :username, :string # This is the driver's username

    timestamps()
  end

  @doc false
  def changeset(taxi, attrs) do
    taxi
    |> cast(attrs, [:username, :location, :status])
    |> validate_required([:username, :location, :status])
  end
end
