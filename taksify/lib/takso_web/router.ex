defmodule TaksoWeb.Router do
  use TaksoWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug CORSPlug, origin: "*"
    plug :accepts, ["json"]
  end

  scope "/", TaksoWeb do
    pipe_through :browser # Use the default browser stack
    get "/", CustomerController, :landing
    get "/login", CustomerController, :login
    get "/booking",CustomerController , :booking
    resources "/users", UserController
    resources "/bookings", BookingController

  end

  # Other scopes may use custom stacks.
  scope "/api", TaksoWeb do
    pipe_through :api

    post "/bookings", Api.BookingController, :create
    post "/sessions", Api.SessionController, :create
    post "/addTaxi", Api.TaxiController, :create
  end

  scope "/api/bookings", TaksoWeb do
    pipe_through :api

    post "/create", Api.BookingController, :create
    post "/delete", Api.BookingController, :delete
    #post "/update", Api.BookingController, :update
    #post "/show", Api.BookingController, :show
  end

  scope "/api/users", TaksoWeb do
    pipe_through :api

    post "/create", Api.UserController, :create
    post "/delete", Api.UserController, :delete
    post "/update", Api.UserController, :update
    post "/show", Api.UserController, :show
  end

end
