defmodule TaksoWeb.TaxiAllocator do
    use GenServer

    def start_link(request, booking_reference) do
        GenServer.start_link(TaksoWeb.TaxiAllocator, request, name: booking_reference)
    end

    def init(request) do
        Process.send_after(self(), :notify_customer, 10000)
        {:ok, request}
    end
    
    def handle_info(:notify_customer, request) do
        TaksoWeb.Endpoint.broadcast("customer:lobby", "requests", %{msg: "We found your taxi ... it will be soon to the pickup address"})
    end
end