defmodule TaksoWeb.CustomerController do
  use TaksoWeb, :controller

  def landing(conn, _params) do
    render conn, "landing.html"
  end

  def login(conn, _params) do
    render conn, "login.html"
  end

  def booking(conn, _params) do
    render conn, "booking.html"
  end
  
end
