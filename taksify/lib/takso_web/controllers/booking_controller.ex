defmodule TaksoWeb.BookingController do
  use TaksoWeb, :controller
  import Ecto.Query, only: [from: 2]

  alias Takso.{Repo, Sales.Taxi, Accounts.User}

  def index(conn, _params) do
    render conn, "index.html"
  end

  def new(conn, _params) do
    render conn, "new.html", changeset: Taxi.changeset(%Taxi{}, %{})
  end

  def create(conn, params) do
    IO.puts "==================================="
    IO.inspect params
    # user = Repo.all(from u in User, where: u.username == ^params.username, select: u)
    # IO.inspect user
    IO.inspect Repo.all(Taxi)
    IO.puts "=--================================"

    query = from t in Taxi, 
            where: t.status == "available", 
            select: t

    available_taxis = Repo.all(query)
    if (length(available_taxis) > 0) do
        conn = put_flash(conn, :error, "Your taxi will arrive in 10 minutes")
        redirect(conn, to: booking_path(conn, :index))
    else
      conn = put_flash(conn, :error, "oops")
      redirect(conn, to: booking_path(conn, :index))
    end
  end
end
