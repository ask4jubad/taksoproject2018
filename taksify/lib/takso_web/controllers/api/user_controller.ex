defmodule TaksoWeb.Api.UserController do
  use TaksoWeb, :controller
  import Ecto.Query, only: [from: 2]
  import Ecto.Changeset
  alias Takso.{Repo, Accounts.User}



  def create(conn, %{"user" => user_params}) do
    IO.inspect user_params
    changeset = User.changeset(%User{}, user_params)
    Repo.insert(changeset)
    put_status(conn, 201)
      |> json(%{msg: "User created Successfully."})
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Repo.get!(User, id)
    changeset = User.changeset(user, user_params)

    Repo.update(changeset)
    put_status(conn, 201)
      |> json(%{msg: "User updated Successfully."})
  end

  def show(conn, %{"id" => id}) do
    user = Repo.get!(User, id)
    put_status(conn, 201)
      |> json(user)
  end

  def delete(conn, %{"id" => id}) do
    user = Repo.get!(User, id)
    Repo.delete!(user)

    put_status(conn, 201)
      |> json(%{msg: "User Deleted Sucessfully."})
  end

  def login(conn, params) do
    username = params["username"]
    pass = params["password"]
    user = Repo.one(from u in User, where: u.username == ^username,where: u.password == ^pass, select: u)
    put_status(conn, 201)
      |> json(%{msg: user.id})
  end
end
