defmodule TaksoWeb.Api.TaxiController do
  use TaksoWeb, :controller
  import Ecto.Query, only: [from: 2]
  import Ecto.Changeset


  alias Takso.{Repo, Sales.Taxi, Sales.Booking, Accounts.User}

  def create(conn, %{"taxi" => taxi_params}) do
    IO.inspect taxi_params
    changeset = Taxi.changeset(%Taxi{}, taxi_params)
    Repo.insert(changeset)
  end
end
