defmodule TaksoWeb.Api.BookingController do
  use TaksoWeb, :controller
  import Ecto.Query, only: [from: 2]
  import Ecto.Changeset


  alias Takso.{Repo, Sales.Taxi, Sales.Booking, Accounts.User}

  def create(conn, params) do
    IO.puts "==================================="
    IO.inspect params
    username = params["username"]
    user = Repo.one(from u in User, where: u.username == ^username, select: u)
    IO.inspect user
    IO.inspect Repo.all(Taxi)
    IO.puts "=--================================"

    TaksoWeb.Endpoint.broadcast("driver:lobby", "requests", params["booking"])
    TaksoWeb.TaxiAllocator.start_link(params["booking"], String.to_atom("BK-#{123}"))

    booking_params = params["booking"]

    booking_changeset = Booking.changeset(%Booking{}, booking_params)
                        |> put_change(:user_id, user.id)
                        # |> put_change(:status, "cancelled")

    # booking_changeset = build_assoc(user, :bookings, Enum.map(booking_params, fn({key, value}) -> {String.to_atom(key), value} end))

    Repo.insert(booking_changeset)

    query = from t in Taxi, where: t.status == "available", select: t
    available_taxis = Repo.all(query)

    if length(available_taxis) > 0 do
      put_status(conn, 201)
      |> json(%{msg: "Your taxi will arrive soon"})
    else
      put_status(conn, 201)
      |> json(%{msg: "We cannot serve your request in this moment"})
    end
  end

  def delete(conn,params) do
      booking = Repo.get!(Booking,params["id"])
      Repo.delete(booking)
      put_status(conn, 201)
      |> json(%{msg: "Booking Deleted Successfully."})
  end
end
