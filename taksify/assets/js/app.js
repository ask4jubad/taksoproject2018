import Vue from "vue";
import VueRouter from 'vue-router';
import axios from "axios";
import socket from "../js/socket"

import customer from "./components/customer";
import driver from "./components/driver";
import map from "./components/map";
import landingPage from "./components/landingPage";
import login from "./components/login";
// import VueGoogleAutocomplete from 'vue-google-autocomplete';
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.component("customer", customer);
Vue.component("driver", driver);
Vue.component("usermap", map);
Vue.component("landingpage", landingPage);
Vue.component("login", login);
// Vue.component("testmap", VueGoogleAutocomplete);

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDsijdFyCJOLlfFd98w1yhHDOl54-8K7Nk',
    libraries: 'places'
  }
})

Vue.use(VueRouter)


const routes = new VueRouter ({
    mode: 'history',
    routes: [
    // define the root url of the application.
    { path: '/' ,  component: landingPage },
    // route for the about route of the web page
    { path: '/login', component: login},

    { path: '/customer', component: customer},
  
    { path: '/driver', component: driver}
  ]
})
  
  // Create the router instance and pass the `routes` option
  // You can pass in additional options here, but let's
  // keep it simple for now.
  const router = new VueRouter({
    routes, // short for routes: routes
    mode: 'history'
  })


export default new Router({
  routes: [
    {
      path: '/',
      name: 'landingpage',
      component: landingpage
    },

    {
        path: '/login',
        name: 'login',
        component: login
    }
  ]
})


new Vue({}).$mount("#takso-app"); 