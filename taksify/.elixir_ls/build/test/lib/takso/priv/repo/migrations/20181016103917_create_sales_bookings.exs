defmodule Takso.Repo.Migrations.CreateSalesBookings do
  use Ecto.Migration

  def change do
    create table(:sales_bookings) do
      add :pickup_address, :string
      add :dropoff_address, :string

      timestamps()
    end

  end
end
